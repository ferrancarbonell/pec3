﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
	public Text peachCounterText;
	public Text timeText;
	
	public static int peachCounter;
	public static string finishTime;
	public static Transform startPosition;
	public static float startTime;

	void Start()
	{
		// Inicializamos el contador de melocotones y guardamos el valor del tiempo inicial
		peachCounter = 0;
		startTime = Time.time;
		// Si antes se activó un checkpoint se recupera el tiempo anterior como tiempo inicial
		if (CheckpointManager.getCheckpoint)
			startTime = startTime - CheckpointManager.timeCheckpoint;
		
		HeroPosition();
	}

	void Update ()
	{
		// Actualizamos el contador de melocotones y el contador del tiempo transcurrido
		peachCounterText.text = "" + peachCounter.ToString();
		timeText.text = (Time.time - startTime).ToString("00") + "s";

		// Si el jugador completa el nivel guardamos el tiempo que ha tardado en completarlo
		if (HeroController.finish)
		{
			finishTime = (Time.time - startTime).ToString("00") + "s";
			CheckpointManager.getCheckpoint = false;
			SceneManager.LoadScene("Winner");
		}

		// Si el jugador está muerto cargamos la escena de Game Over
		if (HeroController.gameOver)
			SceneManager.LoadScene("Game Over");

	}
	// Función para cargar la posición inicial del jugador des del principio o des de un checkpoint
	void HeroPosition()
	{
		startPosition = GameObject.Find("Hero").GetComponent<Transform>();

		if (CheckpointManager.getCheckpoint)
			startPosition.position = CheckpointManager.lastCheckpoint.position;
	}
}
