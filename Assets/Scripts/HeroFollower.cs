﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroFollower : MonoBehaviour {

    public Transform hero;
    private Vector3 position;

    void Update()
    {
        // Asigna la posición del eje X del jugador más un desplazamiento
        position = transform.position;
        position.x = hero.transform.position.x + 3.5f;
        transform.position = position;
    }
}
