﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
	public GameObject checkpointText;
	public AudioClip audioCheckpoint;

	Animator anim;
	Transform getPosition;
	AudioSource audioSource;

	void OnTriggerEnter2D(Collider2D collision)
	{
		// Si le colisiona el jugador guarda la posición y el tiempo transcurrido hasta el momento
		// También carga la animación del texto de "checkpoint" y el audio correspondiente
		if(collision.tag == "Hero")
		{
			anim = GameObject.Find("CheckpointText").GetComponent<Animator>();
			anim.SetTrigger("Get");
			audioSource = GetComponent<AudioSource>();
			audioSource.PlayOneShot(audioCheckpoint, 0.8f);
			getPosition = GameObject.Find("Hero").GetComponent<Transform>();
			CheckpointManager.lastCheckpoint.position = new Vector2(getPosition.position.x+1, getPosition.position.y + 1);
			CheckpointManager.getCheckpoint = true;
			CheckpointManager.timeCheckpoint = Time.time - GameplayManager.startTime;
			Destroy(GetComponent<BoxCollider2D>());
		}
	}
}
