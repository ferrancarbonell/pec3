﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnemyController : MonoBehaviour
{

	public float maxSpeed = 3f;
	public float move = 1f;
	public GameObject shit;
	public AudioClip audioMakeShit;

	Rigidbody2D myRigidbody2D;
	Transform myTransform;
	AudioSource audioSource;
	SpriteRenderer mySpriteRenderer;

	void Start ()
	{
		// Cargamos los valores de los componentes
		myRigidbody2D = GetComponent<Rigidbody2D>();
		myTransform = GetComponent<Transform>();
		audioSource = GetComponent<AudioSource>();
		mySpriteRenderer = GetComponent<SpriteRenderer>();

	}

	void FixedUpdate () 
	{
		// Mueve el enemigo de forma perpetua a un lado u otro dependiendo de la dirección de la imagen
		if (mySpriteRenderer.flipX)
			myRigidbody2D.velocity = new Vector2 (move * maxSpeed*-1, myRigidbody2D.velocity.y);
		else
			myRigidbody2D.velocity = new Vector2 (move * maxSpeed, myRigidbody2D.velocity.y);
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		// Deja caer una caca si detecta el jugador por debajo
		if(collision.tag == "Hero")
		{
			GameObject shitInstance = Instantiate(shit, transform.position, transform.rotation) as GameObject;
			if (mySpriteRenderer.flipX)
				shitInstance.transform.position = new Vector2(myTransform.position.x-0.5f,myTransform.position.y-0.8f);
			else
				shitInstance.transform.position = new Vector2(myTransform.position.x+0.5f,myTransform.position.y-0.8f);

			audioSource.PlayOneShot(audioMakeShit, 0.5f);
		}
	}

}
