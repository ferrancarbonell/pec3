﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    public float maxSpeed = 7f;
    public float jumpForce = 70f;
    public Transform checkGround;
    public Transform aliveTransform;
    public enum HeroState {alive,death,powerup,transition}
    public enum HeroGun {none,shotgun}
    public enum HeroActions {none,jump,fire}
    public HeroGun heroGun;
    public HeroActions heroActions;
    public GameObject bullet;
    public AudioClip audioJumpAlive;
    public AudioClip audioJumpPowerup;
    public AudioClip audioDie;
    public AudioClip audioHurt;
    public AudioClip audioPowerup;
    public AudioClip audioYhaaAlive;
    public AudioClip audioYhaaPowerup;
    public AudioClip audioShoot;
    public AudioClip audioReload;

    public static HeroState heroState;
    public static bool finish;
    public static bool gameOver;
    public static bool facingRight = true;

    bool grounded = false;
    bool airCollision = false;
    float groundRadius = 0.2f;
    
    Animator anim;
    Animator animShotgun;
    Rigidbody2D myRigidbody2D;
    AudioSource audioSource;
    SpriteRenderer spriteRenderer;
    GameObject treasure;

    void Start()
    {
        // Inicializamos los valores de la partida y asignamos el estado inicial del jugador
        finish =  false;
        gameOver = false;
        heroState = HeroState.alive;
        heroGun = HeroGun.none;
        heroActions = HeroActions.none;
        treasure = GameObject.FindGameObjectWithTag("Treasure");
        // Cargamos los valores de los componentes
        anim = GetComponent<Animator>();
        myRigidbody2D = GetComponent<Rigidbody2D>();
        aliveTransform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate ()
    {
        // Si el jugador no está muerto puede moverse
        if (heroState != HeroState.death)
        {
            // Comprovamos si el jugador toca el suelo para poder saltar
            grounded = Physics2D.OverlapCircleAll(checkGround.position, groundRadius).Length > 3;
            anim.SetBool("Ground", grounded);
            anim.SetFloat("vSpeed", myRigidbody2D.velocity.y);
            
            // Si no existe una colision en el aire el jugador se puede mover horizontalmente
            if (airCollision == false)
            {
                float move = Input.GetAxis ("Horizontal");
                anim.SetFloat("Speed",Mathf.Abs(move));
                myRigidbody2D.velocity = new Vector2 (move * maxSpeed, myRigidbody2D.velocity .y);
                // Dependiento del sentido de movimiento se gira la imagen del jugador    
                if (move > 0 &&!facingRight)
                Flip();
                else if (move < 0 && facingRight)
                Flip();
            }
            // Si toca el suelo el jugador salta o dispara
            if (grounded)
            { 
                if(heroActions == HeroActions.jump)
                    Jump();
                if(heroActions == HeroActions.fire)
                    Fire();
            }  
        }
    }

    void Update ()
    {
        // Si el jugador pulsa espacio salta y si pulsa F y tiene escopeta dispara
        if(Input.GetKeyDown(KeyCode.Space))
            heroActions = HeroActions.jump;
        if(Input.GetKeyDown(KeyCode.F))
            if (heroGun == HeroGun.shotgun)
                heroActions = HeroActions.fire;    
    }

    // Función para girar la imagen del jugador
    void Flip()
    {
        facingRight = !facingRight;
        spriteRenderer.flipX = !facingRight;
    }
    // Función para saltar
    void Jump()
    {
        anim.SetBool("Ground", false);
        myRigidbody2D.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        // Suelta un grito agudo o grave dependiento de si tiene powerup
        if(heroState == HeroState.alive)
            audioSource.PlayOneShot(audioJumpAlive, 0.5f);
        if(heroState == HeroState.powerup)
            audioSource.PlayOneShot(audioJumpPowerup, 0.5f);
        heroActions = HeroActions.none;
    }
    // Función para disparar una bala dependiendo de la dirección donde apunta el jugador
    void Fire()
    {
        anim.SetTrigger("Shoot");
        GameObject bulletInstance = Instantiate(bullet, transform.position, transform.rotation) as GameObject;
        if (HeroController.facingRight)
        {
            bulletInstance.transform.position = new Vector2(myRigidbody2D.transform.position.x +1.2f,myRigidbody2D.transform.position.y);
        }
        else
        {
            bulletInstance.transform.position = new Vector2(myRigidbody2D.transform.position.x -1.2f,myRigidbody2D.transform.position.y);
        }
        audioSource.PlayOneShot(audioShoot, 0.8f);
        heroActions = HeroActions.none;
    }

    // Cuando al jugador le colisionan varios objetos
    void OnTriggerEnter2D(Collider2D collision)
    {
        // Cuando la colisiones no sean del mismo jugador
        if (collision.gameObject != gameObject)
        {
            // Si le colisiona la parte que hiere del enemigo
            if (collision.tag == "Finish")
            {
                // Si el jugador está vivo muere
                if(heroState == HeroState.alive)
                    StartCoroutine(Death());
                // Si el jugador tiene powerup lo pierde y entra en transición
                else if(heroState == HeroState.powerup)
                {
                    transform.localScale = new Vector2 (0.9f, 0.9f);
                    StartCoroutine(Transition());
                } 
            }
            // Si le colisiona la zona muerta el jugador muere
            if (collision.tag == "DeathZone")
                StartCoroutine(Death()); 
            // Si le colisiona un powerup cambia de estado a powerup y augmenta tamaño
            if (collision.tag == "Powerup" && heroState != HeroState.powerup)
            {
                heroState = HeroState.powerup;
                audioSource.PlayOneShot(audioPowerup, 0.8f); 
                transform.localScale = new Vector2 (aliveTransform.localScale.x, aliveTransform.localScale.y)* 1.2f;
            }
            // Si le colisiona una escopeta la coje
            if (collision.tag == "Shotgun")
            {
                animShotgun = GameObject.Find("ShootText").GetComponent<Animator>();
                heroGun = HeroGun.shotgun;
                anim.SetBool("Shotgun", true);
                animShotgun.SetTrigger("Get");
                audioSource.PlayOneShot(audioReload, 0.2f); 
            }
            // Si el jugador colisiona con la llave gana la partida
            if (collision.tag == "Key")
                StartCoroutine(Winner());
        }
    }

    // Se activa la colisión en el aire cuando entra en colisión y no se toca el suelo
    void OnCollisionEnter2D(Collision2D other)
	{
        if (grounded == false)
        airCollision = true;
        // Si la colision es contra un enemigo y el jugador está vivo rebota hacia arriba
        if (other.gameObject.tag == "Enemy" && heroState != HeroState.death)
            myRigidbody2D.AddForce(new Vector2(0, 10f), ForceMode2D.Impulse); 
	}

    // Se activa la colisión en el aire mientras se está en colisión y no se toca el suelo
    void OnCollisionStay2D(Collision2D other)
    {
        if (grounded == false)
        airCollision = true;
    }

    // Se desactiva la colisión en el aire cuando se sale de la colisión
    void OnCollisionExit2D(Collision2D other)
    {
        if (airCollision == true)
            airCollision = false;

    }

    // Corutina para mostrar la animación de la muerte del personaje y asignar
    // que ha acabado el juego
    IEnumerator Death()
    {
        anim.SetTrigger("Death");
        heroState = HeroState.death;
        audioSource.PlayOneShot(audioDie, 0.5f);
        yield return new WaitForSeconds(1);
        gameOver = true;  
    }

    // Corutina para quitar la llave cogida, abrir el cofre, soltar un grito
    // final dependiendo de si tiene powerup y asignar que se ha terminado el nivel
    IEnumerator Winner()
    {
        Destroy(GameObject.FindWithTag("Key"));
        treasure.GetComponent<Animator>().enabled = true;
        treasure.GetComponent<AudioSource>().enabled = true;
        if (heroState == HeroState.alive)
        audioSource.PlayOneShot(audioYhaaAlive, 0.7f);
        else
        audioSource.PlayOneShot(audioYhaaPowerup, 0.7f);
        yield return new WaitForSeconds(1f);
        finish = true;  
    }

    // Corutina para la transición cuando el personaje es herido y tiene powerup
    IEnumerator Transition()
    {
        audioSource.PlayOneShot(audioHurt, 0.5f);
        anim.SetBool("Transition", true);
        heroState = HeroState.transition;
        heroGun = HeroGun.none;
        anim.SetBool("Shotgun", false);
        yield return new WaitForSeconds(2);
        anim.SetBool("Transition", false);
        heroState = HeroState.alive;
    }
}