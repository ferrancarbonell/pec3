﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class GameOptions : MonoBehaviour
{
    // Función para cargar la escena de juego
    public void LoadingNewGame()
    {
        SceneManager.LoadScene("Game");
    }
    
    // Funicón para salir del juego
    public void Exit()
    {
        Application.Quit();
    }
}