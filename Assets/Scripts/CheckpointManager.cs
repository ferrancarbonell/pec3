﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
	public static Transform lastCheckpoint;
	public static bool getCheckpoint = false;
	public static float timeCheckpoint;

	void Start()
	{
		// Cargamos un valor inicial de transformación
		lastCheckpoint = GetComponent<Transform>();
	}
	void Awake()
    {
		// Guardamos los valores de las variables cuando se continua la partida des de un checkpoint
        DontDestroyOnLoad(this.gameObject);
    }
}
