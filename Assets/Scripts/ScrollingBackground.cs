﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour {

	public float backgroundSize;
	public float parallaxSpeed;

	Transform cameraTransform;
	Transform[] layers;
	float viewZone = 10;
	float lastCameraX;
	int leftIndex;
	int rightIndex;

	void Start() 
	{
		// Asignamos los valores de transformación de la cámara y cargamos los fondos en un array
		cameraTransform = Camera.main.transform;
		lastCameraX = cameraTransform.position.x;
		layers = new Transform[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			layers[i] = transform.GetChild(i);
		}

		leftIndex = 0;
		rightIndex = layers.Length-1;
	}

	void Update() 
	{
		// Movemos los fondos cuando la cámara se mueve horizontalmente
		float deltaX = cameraTransform.position.x - lastCameraX;
		transform.position += Vector3.right * (deltaX * parallaxSpeed);
		lastCameraX = cameraTransform.position.x;
		// Dependiendo de si la cámara se mueve en una dirección u otra se activa
		// el scroll correspondiente
		if (cameraTransform.position.x < (layers[leftIndex].transform.position.x + viewZone))
			ScrollLeft();
		if (cameraTransform.position.x > (layers[rightIndex].transform.position.x - viewZone))
			ScrollRight();
	}
	// Función encargada de cambiar la posición del fondo de más a la derecha hacia la izquierda
	void ScrollLeft()
	{
		layers[rightIndex].position = Vector3.right * (layers[leftIndex].position.x - backgroundSize);
		leftIndex = rightIndex;
		rightIndex --;
		if (rightIndex < 0)
		{
			rightIndex = layers.Length-1;
		}
	}
	// Función encargada de cambiar la posición del fondo de más a la izquierda hacia la derecha
	void ScrollRight()
	{
		layers[leftIndex].position = Vector3.right * (layers[rightIndex].position.x + backgroundSize);
		rightIndex = leftIndex;
		leftIndex ++;
		if (leftIndex == layers.Length)
		{
			leftIndex = 0;
		}
	}
}
