﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
	public float maxSpeed = 10f;
	public float move = 1f;

	int face;
	Rigidbody2D myRigidbody2D;

	void Start ()
	{
		// Cargamos los valores de los componentes
		myRigidbody2D = GetComponent<Rigidbody2D>();
		if (HeroController.facingRight)
			face = 1;
		else
			face = -1;
	}
	void FixedUpdate () 
	{
		// Mueve la bala de forma perpetua hasta que colisione
		myRigidbody2D.velocity = new Vector2 (move * maxSpeed * face, myRigidbody2D.velocity.y);
	}
	// Cuando detecta un collider la bala desaparece
	void OnTriggerEnter2D(Collider2D collision)
    {
		if (collision.tag != "Hero" && collision.tag != "Killer" && collision.tag != "FlyEnemy")
			Destroy(gameObject);
    }
}
