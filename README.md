<h1>PEC3 - Jungle Cowboy v.2.0</h1>
<p>Ésta práctica consiste en la ampliación de Jungle Cowboy de la práctica anterior PEC2. En éste caso con las siguientes mejoras</p>
<ul>
<li><strong>Añadido enemigo volador que daña con su caca</li>
<li><strong>Añadida lluvia como sistema de partículas</li>
<li><strong>Añadida escopeta</li>
<li><strong>Añadidos otros tiles para el tilemap</li>
<li><strong>Añadidos 3 checkpoints</li>
<li><strong>Algunos bugs solucionados</li>
<li><strong>Optimización del comportamiento y animación de los enemigos de tierra</li>
<li><strong>Nuevo diseño de nivel</li>
</ul>

<h2>Implementación y estructuración</h2>
<p>Éste juego se ha desarollado con Unity en la versión 2018.2.14.</p>

<p>Los assets del proyecto se dividen en:</p>
<h3>Scenes</h3>
<p>Se estructura en 4 escenas:</p>
<ul>
<li><strong>Start:</strong> escena inicial para cargar una nueva partida o salir del juego.</li>
<li><strong>Game:</strong> escena de juego donde hay que superar el nivel a través del movimiento del personaje.</li>
<li><strong>Winner:</strong> escena final con los melocotones recogidos y tiempo transcurrido en superar el nivel. También se puede volver a jugar o salir del juego.</li>
<li><strong>Game Over:</strong> escena que aparece cuando el personaje muere y se puede volver a jugar des de un chekpoint si se ha alcanzado o salir del juego</li>
</ul>

<h3>Audio</h3>
<p>Todos los archivos de audio utilizados entre otros de pruebas. algunos son de creación propia y otros de recursos externos.</p>

<h3>Fonts</h3>
<p>Única fuente tipográfica utilizada</p>

<h3>Scripts</h3>
<ul>
<li><strong>Box Controller: </strong> define el comportamiento de las cajas.</li>
<li><strong>Bullet Behaviour: </strong> define el comportamiento de las balas.</li>
<li><strong>Camera move: </strong> mueve la cámara hacia a la derecha de forma perpetua en todas las escenas excepto la de juego.</li>
<li><strong>Checkpoint: </strong> define que ocurre cuando se activa un checkpoint.</li>
<li><strong>Checkpoint Manager: </strong> controla los valores optenidos en un checkpoint.</li>
<li><strong>Enemy AI: </strong> define el comportamiento de los enemigos.</li>
<li><strong>Fly Enemy Controller: </strong> define el comportamiento de los enemigos voladores.</li>
<li><strong>Game Options: </strong> define las opciones de los botones de la interfície de juego.</li>
<li><strong>Gameplay Manager: </strong> define el comportamiento entre escenas.</li>
<li><strong>Hero Controller: </strong> define el comportamiento del héro que controla el jugador.</li>
<li><strong>Hero Follower: </strong> pone el target a seguir de la cámara principal en la escena de juego.</li>
<li><strong>Powerup: </strong> elimina el powerup cuando el jugador lo recoge.</li>
<li><strong>Scrolling Background: </strong> define un efecto parallax entre los elementos de la escena, el background y el foreground.</li>
<li><strong>Shit Behaviour: </strong> define el comportamiento de las cacas de los enemigos voladores.</li>
<li><strong>Winner: </strong> carga los datos de melocotones y tiempo en la pantalla final una vez superado el nivel.</li>
</ul>

<h3>Prefabs</h3>
<p>Donde se encuentran los prefabs de enemigos, balas, cacas, cajas y algunos elementos de decoración.</p>

<h3>Sprites</h3>
<p>Las imágenes, tilemaps y paletas de sprites del juego organizados en: </p>
<ul>
<li>Background</li>
<li>Box</li>
<li>Bullets</li>
<li>Enemy</li>
<li>Hero</li>
<li>Objects</li>
<li>Sprites Palettes</li>
<li>Tilemaps</li>
</ul>

<h3>Animations</h3>
<p>Todas las animaciones del juego organizadas por:</p>
<ul>
<li>Hero</li>
<li>Enemy</li>
<li>Box</li>
<li>Bullets</li>
<li>Camera</li>
<li>Text</li>
<li>Treasue</li>
</ul>

<h3>Materials</h3>
<p>Donde se situan los dos materiales del juego sin fricción</p>

<h2>Recursos externos</h2>
<ul>
<li><strong>Dinosaur Park </strong>(GameArt2D.com) https://www.gameart2d.com/dinosaur-park-game-sprites.html</li>
<li><strong>Fantasy Sfx </strong>(LITTLE ROBOT SOUND FACTORY) https://assetstore.unity.com/packages/audio/sound-fx/fantasy-sfx-32833a</li>
<li><strong>Tribal Jungle Music Free Pack </strong>(TYLER CUNNINGHAM) https://assetstore.unity.com/packages/audio/music/tribal-jungle-music-free-pack-131414</li>
<li><strong>Cartoon Game Sound </strong>(J.BOB SOUND STUDIO) https://assetstore.unity.com/packages/audio/sound-fx/cartoon-game-sound-5438</li>
<li><strong>AlfaSlabOne-Regular </strong>(JM Solé) https://fonts.google.com/specimen/Alfa+Slab+One</li>
<li><strong>Post Apocalypse Guns Demo </strong>(SOUND EARTH GAME AUDIO) https://assetstore.unity.com/packages/audio/sound-fx/weapons/post-apocalypse-guns-demo-33515</li>
</ul>

<h2>Como jugar</h2>
<p>Para jugar en la pantalla principal hay que clicar a "nueva partida". El objetivo principal es recoger el máximo de melocotones y completar el nivel el más rápido posible.</p>
<p>El personaje se controla con las flechas horizontales y el espacio para saltar. En esta versión también se puede disparar con F si se obtiene la escopetar</p>
<p>En el nivel hay que sortear obstáculos y enemigos que se pueden eliminar saltando sobre ellos. En esta versión también existen enemigos voladores que dejan caer cacas dañinas al detectar al jugador. Estos enemigos no se pueden eliminar. También existen uns bloques que se pueden activar para conseguir melocotones o powerups. Al final se encuentra la llave que abrirá el cofre y finalizará la partida.</p>

<p>link del gameplay: https://vimeo.com/311045851</p>
<h2>Bugs Conocidos</h2>
<p>Alguna vez el personaje se queda parado indefinidamente aún con material con 0 de fricción añadido.</p>
<p>Algun tile no está bien recortado y se muestra un poco de otro</p>